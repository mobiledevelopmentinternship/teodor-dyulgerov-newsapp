package com.teodordyulgerov.newsly.ui.fragment;

public interface BaseView<T> {

    void setPresenter(T presenter);
}
