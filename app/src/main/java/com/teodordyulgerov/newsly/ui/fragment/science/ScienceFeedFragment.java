package com.teodordyulgerov.newsly.ui.fragment.science;

import com.teodordyulgerov.newsly.App;
import com.teodordyulgerov.newsly.R;
import com.teodordyulgerov.newsly.presenter.science.ScienceFeedContract;
import com.teodordyulgerov.newsly.ui.fragment.BaseNewsFeedFragment;

public class ScienceFeedFragment extends BaseNewsFeedFragment
        implements ScienceFeedContract.View {

    public static ScienceFeedFragment newInstance() {
        return new ScienceFeedFragment();
    }

    @Override
    public String getFragmentTag() {
        return App.getInstance().getString(R.string.tab_title_science);
    }
}
