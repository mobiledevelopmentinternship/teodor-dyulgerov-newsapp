package com.teodordyulgerov.newsly.ui.fragment.technology;

import com.teodordyulgerov.newsly.App;
import com.teodordyulgerov.newsly.R;
import com.teodordyulgerov.newsly.presenter.technology.TechnologyFeedContract;
import com.teodordyulgerov.newsly.ui.fragment.BaseNewsFeedFragment;

public class TechnologyFeedFragment extends BaseNewsFeedFragment
        implements TechnologyFeedContract.View {

    public static TechnologyFeedFragment newInstance() {
        return new TechnologyFeedFragment();
    }

    @Override
    public String getFragmentTag() {
        return App.getInstance().getString(R.string.tab_title_technology);
    }
}
