package com.teodordyulgerov.newsly.ui.fragment.personalized;

import com.teodordyulgerov.newsly.presenter.personalized.PersonalizedFeedContract;
import com.teodordyulgerov.newsly.ui.fragment.BaseNewsFeedFragment;

public class PersonalizedFeedFragment extends BaseNewsFeedFragment
        implements PersonalizedFeedContract.View {

    public static PersonalizedFeedFragment newInstance() {
        return new PersonalizedFeedFragment();
    }
}
