package com.teodordyulgerov.newsly.ui.fragment.business;

import com.teodordyulgerov.newsly.App;
import com.teodordyulgerov.newsly.R;
import com.teodordyulgerov.newsly.presenter.business.BusinessFeedContract;
import com.teodordyulgerov.newsly.ui.fragment.BaseNewsFeedFragment;

public class BusinessFeedFragment extends BaseNewsFeedFragment
        implements BusinessFeedContract.View {

    public static BusinessFeedFragment newInstance() {
        return new BusinessFeedFragment();
    }

    @Override
    public String getFragmentTag() {
        return App.getInstance().getString(R.string.tab_title_business);
    }
}
