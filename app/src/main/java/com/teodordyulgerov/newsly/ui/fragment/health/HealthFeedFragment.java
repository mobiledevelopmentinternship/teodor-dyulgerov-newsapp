package com.teodordyulgerov.newsly.ui.fragment.health;

import com.teodordyulgerov.newsly.App;
import com.teodordyulgerov.newsly.R;
import com.teodordyulgerov.newsly.presenter.health.HealthFeedContract;
import com.teodordyulgerov.newsly.ui.fragment.BaseNewsFeedFragment;

public class HealthFeedFragment extends BaseNewsFeedFragment
        implements HealthFeedContract.View {

    public static HealthFeedFragment newInstance() {
        return new HealthFeedFragment();
    }

    @Override
    public String getFragmentTag() {
        return App.getInstance().getString(R.string.tab_title_health);
    }
}
