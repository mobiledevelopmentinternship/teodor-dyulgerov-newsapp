package com.teodordyulgerov.newsly.ui.fragment.sports;

import com.teodordyulgerov.newsly.App;
import com.teodordyulgerov.newsly.R;
import com.teodordyulgerov.newsly.presenter.sports.SportsFeedContract;
import com.teodordyulgerov.newsly.ui.fragment.BaseNewsFeedFragment;

public class SportsFeedFragment extends BaseNewsFeedFragment
        implements SportsFeedContract.View {

    public static SportsFeedFragment newInstance() {
        return new SportsFeedFragment();
    }

    @Override
    public String getFragmentTag() {
        return App.getInstance().getString(R.string.tab_title_sports);
    }
}
