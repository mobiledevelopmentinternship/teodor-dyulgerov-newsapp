package com.teodordyulgerov.newsly.ui.fragment.entertainment;

import com.teodordyulgerov.newsly.App;
import com.teodordyulgerov.newsly.R;
import com.teodordyulgerov.newsly.presenter.entertainment.EntertainmentFeedContract;
import com.teodordyulgerov.newsly.ui.fragment.BaseNewsFeedFragment;

public class EntertainmentFeedFragment extends BaseNewsFeedFragment
        implements EntertainmentFeedContract.View {

    public static EntertainmentFeedFragment newInstance() {
        return new EntertainmentFeedFragment();
    }

    @Override
    public String getFragmentTag() {
        return App.getInstance().getString(R.string.tab_title_entertainment);
    }
}
