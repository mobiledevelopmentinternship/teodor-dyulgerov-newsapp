package com.teodordyulgerov.newsly.adapter.diffutil;

public interface RecyclerViewItemMarker {

    int getUniqueId();
}
