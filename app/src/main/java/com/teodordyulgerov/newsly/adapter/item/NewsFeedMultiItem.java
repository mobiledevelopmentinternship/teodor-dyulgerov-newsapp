package com.teodordyulgerov.newsly.adapter.item;

import com.teodordyulgerov.newsly.adapter.viewholder.NewsFeedViewHolderTypes;
import com.teodordyulgerov.newsly.persistence.model.ArticlePersist;

/**
 * Super class for handling multiple adapter items.
 * See {@link ArticlePersist}
 * and {@link NewsFeedEndItem}.
 */
public abstract class NewsFeedMultiItem implements NewsFeedViewHolderTypes {
}
