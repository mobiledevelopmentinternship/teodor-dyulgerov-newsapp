package com.teodordyulgerov.newsly.presenter.health;

import com.teodordyulgerov.newsly.presenter.BaseNewsFeedContract;

public interface HealthFeedContract extends BaseNewsFeedContract {

    interface View extends BaseNewsFeedContract.View {
        // Not used
    }

    interface Presenter extends BaseNewsFeedContract.Presenter {
        // Not used
    }
}
