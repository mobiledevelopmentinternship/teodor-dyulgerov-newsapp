package com.teodordyulgerov.newsly.presenter;

public interface IBasePresenter {

    void onCreate();

    void onResume();

    void onDestroy();
}
