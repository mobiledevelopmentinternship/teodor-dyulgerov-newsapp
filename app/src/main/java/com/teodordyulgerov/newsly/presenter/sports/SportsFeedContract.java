package com.teodordyulgerov.newsly.presenter.sports;

import com.teodordyulgerov.newsly.presenter.BaseNewsFeedContract;

public interface SportsFeedContract extends BaseNewsFeedContract {

    interface View extends BaseNewsFeedContract.View {
        // Not used
    }

    interface Presenter extends BaseNewsFeedContract.Presenter {
        // Not used
    }
}
