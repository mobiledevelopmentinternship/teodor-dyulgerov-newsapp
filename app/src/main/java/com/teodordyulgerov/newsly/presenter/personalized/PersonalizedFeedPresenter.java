package com.teodordyulgerov.newsly.presenter.personalized;

import com.teodordyulgerov.newsly.network.utils.ApiConstants;
import com.teodordyulgerov.newsly.presenter.BaseNewsFeedPresenter;

public class PersonalizedFeedPresenter extends BaseNewsFeedPresenter
        implements PersonalizedFeedContract.Presenter {

    public PersonalizedFeedPresenter(PersonalizedFeedContract.View view) {
        super(view, ApiConstants.CATEGORY_GENERAL);
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }
}