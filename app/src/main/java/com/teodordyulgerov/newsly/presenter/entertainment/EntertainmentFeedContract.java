package com.teodordyulgerov.newsly.presenter.entertainment;

import com.teodordyulgerov.newsly.presenter.BaseNewsFeedContract;

public interface EntertainmentFeedContract extends BaseNewsFeedContract {

    interface View extends BaseNewsFeedContract.View {
        // Not used
    }

    interface Presenter extends BaseNewsFeedContract.Presenter {
        // Not used
    }
}
