package com.teodordyulgerov.newsly.presenter.science;

import com.teodordyulgerov.newsly.network.utils.ApiConstants;
import com.teodordyulgerov.newsly.presenter.BaseNewsFeedPresenter;

public class ScienceFeedPresenter extends BaseNewsFeedPresenter
        implements ScienceFeedContract.Presenter {

    public ScienceFeedPresenter(ScienceFeedContract.View view) {
        super(view, ApiConstants.CATEGORY_SCIENCE);
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }
}