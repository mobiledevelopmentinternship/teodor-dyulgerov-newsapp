package com.teodordyulgerov.newsly.presenter.entertainment;

import com.teodordyulgerov.newsly.network.utils.ApiConstants;
import com.teodordyulgerov.newsly.presenter.BaseNewsFeedPresenter;

public class EntertainmentFeedPresenter extends BaseNewsFeedPresenter
        implements EntertainmentFeedContract.Presenter {

    public EntertainmentFeedPresenter(EntertainmentFeedContract.View view) {
        super(view, ApiConstants.CATEGORY_ENTERTAINMENT);
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }
}