package com.teodordyulgerov.newsly.presenter.favoritearticles;

import com.teodordyulgerov.newsly.persistence.model.ArticlePersist;
import com.teodordyulgerov.newsly.presenter.BaseFavoriteContract;

import java.util.List;

public interface FavoriteArticlesContract extends BaseFavoriteContract {

    interface View extends BaseFavoriteContract.View {

        void updateRecyclerView(List<ArticlePersist> result);

        void startActivityNewsArticle(ArticlePersist article);
    }

    interface Presenter extends BaseFavoriteContract.Presenter {

        void processArticleClick(ArticlePersist article);
    }
}
