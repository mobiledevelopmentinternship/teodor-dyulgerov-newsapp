package com.teodordyulgerov.newsly.presenter.business;

import com.teodordyulgerov.newsly.presenter.BaseNewsFeedContract;

public interface BusinessFeedContract extends BaseNewsFeedContract {

    interface View extends BaseNewsFeedContract.View {
        // Not used
    }

    interface Presenter extends BaseNewsFeedContract.Presenter {
        // Not used
    }
}
