package com.teodordyulgerov.newsly.presenter.health;

import com.teodordyulgerov.newsly.network.utils.ApiConstants;
import com.teodordyulgerov.newsly.presenter.BaseNewsFeedPresenter;

public class HealthFeedPresenter extends BaseNewsFeedPresenter
        implements HealthFeedContract.Presenter {

    public HealthFeedPresenter(HealthFeedContract.View view) {
        super(view, ApiConstants.CATEGORY_HEALTH);
    }

    @Override
    protected void inject() {
        getAppComponent().inject(this);
    }
}