package com.teodordyulgerov.newsly.presenter.technology;

import com.teodordyulgerov.newsly.presenter.BaseNewsFeedContract;

public interface TechnologyFeedContract extends BaseNewsFeedContract {

    interface View extends BaseNewsFeedContract.View {
        // Not used
    }

    interface Presenter extends BaseNewsFeedContract.Presenter {
        // Not used
    }
}
