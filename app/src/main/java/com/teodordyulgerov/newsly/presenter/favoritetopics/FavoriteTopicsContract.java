package com.teodordyulgerov.newsly.presenter.favoritetopics;

import com.teodordyulgerov.newsly.persistence.model.TopicPersist;
import com.teodordyulgerov.newsly.presenter.BaseFavoriteContract;

import java.util.List;

public interface FavoriteTopicsContract extends BaseFavoriteContract {

    interface View extends BaseFavoriteContract.View {

        void setFabSearchTopicState(boolean isEnabled);

        void updateRecyclerView(List<TopicPersist> result);

        void loadTopicFeedFragment(TopicPersist topic);
    }

    interface Presenter extends BaseFavoriteContract.Presenter {

        void onSearch(String phrase);

        void processTopicClick(TopicPersist topic);
    }
}
