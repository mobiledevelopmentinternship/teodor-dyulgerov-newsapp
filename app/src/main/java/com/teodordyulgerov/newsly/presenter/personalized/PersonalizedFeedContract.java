package com.teodordyulgerov.newsly.presenter.personalized;

import com.teodordyulgerov.newsly.presenter.BaseNewsFeedContract;

public interface PersonalizedFeedContract extends BaseNewsFeedContract {

    interface View extends BaseNewsFeedContract.View {
        // Not used
    }

    interface Presenter extends BaseNewsFeedContract.Presenter {
        // Not used
    }
}
