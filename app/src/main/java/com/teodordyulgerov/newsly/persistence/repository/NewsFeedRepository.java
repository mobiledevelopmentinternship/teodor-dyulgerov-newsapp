package com.teodordyulgerov.newsly.persistence.repository;

import com.teodordyulgerov.newsly.persistence.model.NewsFeedPersist;

public interface NewsFeedRepository extends BaseRepository<NewsFeedPersist> {

    void deleteNewsFeedByTitle(String title);
}
