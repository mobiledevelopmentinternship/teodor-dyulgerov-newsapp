package com.teodordyulgerov.newsly.persistence.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.teodordyulgerov.newsly.persistence.dao.ArticleDao;
import com.teodordyulgerov.newsly.persistence.dao.NewsFeedDao;
import com.teodordyulgerov.newsly.persistence.dao.SourceDao;
import com.teodordyulgerov.newsly.persistence.dao.TopicDao;
import com.teodordyulgerov.newsly.persistence.model.ArticlePersist;
import com.teodordyulgerov.newsly.persistence.model.NewsFeedPersist;
import com.teodordyulgerov.newsly.persistence.model.SourcePersist;
import com.teodordyulgerov.newsly.persistence.model.TopicPersist;

@Database(
        entities = {
                TopicPersist.class,
                ArticlePersist.class,
                NewsFeedPersist.class,
                SourcePersist.class},
        version = 1)
public abstract class NewsDatabase extends RoomDatabase {

    public abstract TopicDao favoriteTopicDao();

    public abstract ArticleDao newsArticleDao();

    public abstract NewsFeedDao newsFeedDao();

    public abstract SourceDao newsSourceDao();
}
