package com.teodordyulgerov.newsly.dagger.component;

import com.teodordyulgerov.newsly.dagger.module.AppModule;
import com.teodordyulgerov.newsly.dagger.module.NetworkModule;
import com.teodordyulgerov.newsly.dagger.module.RoomModule;
import com.teodordyulgerov.newsly.presenter.business.BusinessFeedPresenter;
import com.teodordyulgerov.newsly.presenter.entertainment.EntertainmentFeedPresenter;
import com.teodordyulgerov.newsly.presenter.favoritearticles.FavoriteArticlesPresenter;
import com.teodordyulgerov.newsly.presenter.favoritesourcefeed.FavoriteSourceFeedPresenter;
import com.teodordyulgerov.newsly.presenter.favoritesources.FavoriteSourcesPresenter;
import com.teodordyulgerov.newsly.presenter.favoritetopicfeed.FavoriteTopicFeedPresenter;
import com.teodordyulgerov.newsly.presenter.favoritetopics.FavoriteTopicsPresenter;
import com.teodordyulgerov.newsly.presenter.health.HealthFeedPresenter;
import com.teodordyulgerov.newsly.presenter.newsarticle.NewsArticlePresenter;
import com.teodordyulgerov.newsly.presenter.personalized.PersonalizedFeedPresenter;
import com.teodordyulgerov.newsly.presenter.science.ScienceFeedPresenter;
import com.teodordyulgerov.newsly.presenter.sports.SportsFeedPresenter;
import com.teodordyulgerov.newsly.presenter.technology.TechnologyFeedPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        AppModule.class,
        RoomModule.class,
        NetworkModule.class
})
public interface AppComponent {

    void inject(BusinessFeedPresenter presenter);

    void inject(EntertainmentFeedPresenter presenter);

    void inject(FavoriteArticlesPresenter presenter);

    void inject(FavoriteSourceFeedPresenter presenter);

    void inject(FavoriteSourcesPresenter presenter);

    void inject(FavoriteTopicFeedPresenter presenter);

    void inject(FavoriteTopicsPresenter presenter);

    void inject(HealthFeedPresenter presenter);

    void inject(NewsArticlePresenter presenter);

    void inject(PersonalizedFeedPresenter presenter);

    void inject(ScienceFeedPresenter presenter);

    void inject(SportsFeedPresenter presenter);

    void inject(TechnologyFeedPresenter presenter);
}
