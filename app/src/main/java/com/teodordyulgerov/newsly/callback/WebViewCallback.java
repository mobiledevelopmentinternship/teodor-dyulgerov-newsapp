package com.teodordyulgerov.newsly.callback;

public interface WebViewCallback {

    void onWebViewLoadSuccess();
}
