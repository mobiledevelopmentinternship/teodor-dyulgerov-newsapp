package com.teodordyulgerov.newsly.callback;

import com.teodordyulgerov.newsly.adapter.diffutil.RecyclerViewItemMarker;

public interface OnRecyclerViewItemClickListener<T extends RecyclerViewItemMarker> {

    void onRecyclerViewItemClick(T item);
}
