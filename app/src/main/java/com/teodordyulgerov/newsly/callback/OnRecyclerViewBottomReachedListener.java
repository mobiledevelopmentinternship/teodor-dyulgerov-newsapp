package com.teodordyulgerov.newsly.callback;

public interface OnRecyclerViewBottomReachedListener {

    void onBottomReached();
}
